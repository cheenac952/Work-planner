import React, {useState} from 'react';
import { Link } from 'react-router-dom';
import './AdminNav.css';

function AdminNav() {
    const [dashboard, setDashboard] = useState(true);
    const [project, setProject] = useState(false);
    const [employee, setEmployee] = useState(false);
    const dashboardClick = (event)=>{
        setDashboard(true);
        setEmployee(false);
        setProject(false);
    }
    const employeeClick = (event) =>{
        setDashboard(false);
        setEmployee(true);
        setProject(false);
    }
    const projectClick = (event)=>{
        setDashboard(false);
        setEmployee(false);
        setProject(true);
    }
    return (
             <div className="admin__nav">
                <div className="admin__Wp">
                    <span className="admin__logoWp">Wp</span>
                </div>
                <div className="admin__link">
                    <ul className="admin__ul">
                        <Link to='/admin' style={{textDecoration:"inherit", color:"black"}}><li className={`admin__li ${dashboard ? "text_underline" : "" }`} onClick={dashboardClick}>Dashboard</li></Link>
                        <li className={`admin__li ${project ? "text_underline" : "" }`} onClick={projectClick}>Projects</li>
                       <Link to='/admin/employee' style={{textDecoration:"inherit", color:"black"}}><li className={`admin__li ${employee ? "text_underline" : "" }`} onClick={employeeClick}>Employee List</li></Link> 
                    </ul>
                </div>
                <div className="admin__profile">
                    <span className="admin__org">Amazon</span>
                </div>
            </div>
    )
}

export default AdminNav
