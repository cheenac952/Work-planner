import React  from "react";
import AdminNav from "../AdminNav/AdminNav";
import "./Admin.css";
function Admin() {
    const a = [1,2,3, 4, 5, 6, 7];
  return (
    <div className="admin">
      <AdminNav />
      <div className="admin__about">
        <h5 className="admin__h5">About Company</h5>
        <p className="admin__para">
          Sed ut perspiciatis unde omnis iste natus error sit voluptatem
          accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae
          ab illo inventore veritatis et quasi architecto beatae vitae dicta
          sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit
          aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos
          qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui
          dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed
          quia non numquam eius modi tempora incidunt ut labore et dolore magnam
          aliquam quaerat voluptatem. Ut enim ad minima veniam, quis nostrum
          exercitationem ullam corporis suscipit laboriosam, nisi ut aliquid ex
          ea commodi consequatur? Quis autem vel eum iure reprehenderit qui in
          ea voluptate velit esse quam nihil molestiae consequatur, vel illum
          qui dolorem eum fugiat quo voluptas nulla pariatur ? Quis autem vel
          eum iure reprehenderit qui in ea voluptate velit esse quam nihil
          molestiae consequatur, vel illum qui dolorem eum fugiat quo voluptas
          nulla pariatur ? Quis autem vel eum iure reprehenderit qui in ea
          voluptate velit esse quam nihil molestiae consequatur, vel illum qui
          dolorem eum fugiat quo voluptas nulla pariatur quasi architecto beatae
          vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas
          sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores
          eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est,
          qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit,
          sed quia non numquam eius modi tempora incidunt ut labore et dolore
          magnam aliquam quaerat voluptatem. Ut enim ad minima veniam, quis
          nostrum exercitationem ullam corporis suscipit laboriosam, nisi ut
          aliquid ex ea commodi consequatur? Quis autem vel eum iure
          reprehenderit qui in ea voluptate velit esse quam nihil molestiae
          consequatur, vel illum qui dolorem eum fugiat quo voluptas nulla
          pariatur ? Quis autem vel eum iure reprehenderit qui in ea voluptate
          velit esse quam nihil molestiae consequatur, vel illum qui dolorem eum
          fugiat quo voluptas nulla pariatur ? Quis autem vel eum iure
          reprehenderit qui in ea voluptate velit esse quam nihil molestiae
          consequatur, vel illum qui dolorem eum fugiat quo voluptas nulla
          pariatur
        </p>
      </div>
      <div className="admin__about">
        <h5 className="admin__h5">Projects Completed</h5>  
        <div className="project_complete">
            {
                a.map((n) => {
                    return(
                        <div className="project__card">
                            <h6 className="card_title">Card Title</h6>
                            <p className="card__text">Some quick example text to build on the card title and make up the
                            bulk of the card's content.</p>
                          <button className="project_button">Go somewhere</button>
                            </div>
                        
                    )
                    
                })
            }
       
        </div> 
      </div>

      <div className="admin__about">
        <h5 className="admin__h5">Projects Running</h5>  
        <div className="project_complete">
            {
                a.map((n) => {
                    return(
                        <div className="project__card">
                            <h6 className="card_title">Card Title</h6>
                            <p className="card__text">Some quick example text to build on the card title and make up the
                            bulk of the card's content.</p>
                          <button className="project_button">Go somewhere</button>
                            </div>
                        
                    )
                    
                })
            }
       
        </div> 
      </div>
      
    </div>
  );
}

export default Admin;
