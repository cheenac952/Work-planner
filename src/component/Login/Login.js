
import React, {useState} from 'react'
import './Login.css';
import logo from './logo.svg';
import Modal from "react-bootstrap/Modal";
import { Link } from 'react-router-dom';
function Login() {
    const [show, setShow] = useState(false);

  const handleClose = () => setShow(false);
  const handleShow = () => setShow(true);
    return (
        <>
        <div className="login">
            <div className="login__nav">
                <div className="login__logo">
                <div className="login__logo1">Work</div>
                <div className="login__logo1">Planner<span className="login__dot">.</span></div>
                </div>
                <div className="login__button">
                <button className="login__Join" onClick={handleShow}>Join Now</button>
                
                </div>
             </div>
             <div className="login__body">
                <img src={logo} alt="logo" className="logo"/>
                <div className="login__field">
                    <h3 className="heading">Welcome to your professional community</h3>
                    <div className="login__fieldContainer">
                        <div className="login__inputField">
                        <input type="text" placeholder="Enter Email" className="login__input"/>
                        </div>
                        <div className="login__inputField">
                        <input type="password" placeholder="Enter Password" className="login__input"/>
                        </div>
                        <div className="login__inputField">
                        <input type="text" placeholder="Login as: Employee/Admin/Planner" className="login__input"/>
                        </div>
                        <div className="login__SignInButton">
                          <Link to="/admin"><button className="SignIn">Sign In</button></Link>
                        </div>
                        
                    </div>
                </div>
            </div>
            
        </div>
        <Modal
        show={show}
        onHide={handleClose}
        backdrop="static"
        keyboard={false}
      >
        <Modal.Header closeButton>
          <Modal.Title>Sign Up</Modal.Title>
        </Modal.Header>
        <Modal.Body>
        <div className="login__fieldContainer">
                        <div className="login__inputField">
                        <input type="text" placeholder="Enter Email" className="login__input"/>
                        </div>
                        <div className="login__inputField">
                        <input type="text" placeholder="Enter Organization name" className="login__input"/>
                        </div>
                        <div className="login__inputField">
                        <input type="password" placeholder="Enter Password" className="login__input"/>
                        </div>
                        <div className="login__inputField">
                        <input type="password" placeholder="Re-Enter Password" className="login__input"/>
                        </div>
                        <div className="login__inputField">
                        <input type="text" placeholder="Sign Up as: Employee/Admin/Planner" className="login__input"/>
                        </div>
                        <div className="login__SignInButton">
                        <button className="SignIn" onClick={handleClose}>Join Now</button>
                        </div>
                        
        </div>
        </Modal.Body>
      </Modal>
        </>
    )
}

export default Login
