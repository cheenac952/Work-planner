import './App.css';
import Login from './component/Login/Login';
import {
  BrowserRouter as Router,
  Switch,
  Route
} from "react-router-dom";
import Admin from './component/Admin/Admin';
import AdminEmployee from './component/AdminEmployee/AdminEmployee';

function App() {
  return (
    <div className="App">
      <Router>
        <Switch>
          <Route exact path='/' component={Login}/>
          <Route exact path='/admin' component={Admin}/>
          <Route exact path='/admin/employee' component={AdminEmployee}/>
        </Switch>
      </Router>
    </div>
  );
}

export default App;
